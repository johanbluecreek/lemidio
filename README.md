# Lemidio

A Lemmy Radio project -- Lemidio!

Inspired by [reddytt](https://gitlab.com/johanbluecreek/reddytt).

## Install

Create a venv, activate it and install the requirements

```
$ python -m venv venv
$ . venv/bin/activate
$ python -m pip install -r requirements.txt
```

## Run

You need a Lemmy account to run.

Run the program with (having your venv activated):
```
./lemidio.py
```
