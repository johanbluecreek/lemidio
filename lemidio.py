#!/usr/bin/env python

__version__ = "0.1.0"
APPNAME = "lemidio"

# builtins
import subprocess
import os
import getpass
import base64
import pickle
import gzip
import json
# third party
from platformdirs import user_config_dir
from plemmy import LemmyHttp


MIN_UNSEEN = 5
MORE_UNSEEN = 9
LIMIT = 10

class Cred():
    def __init__(self, cred_data):
      self.user = cred_data.get('user')
      self.password = cred_data.get('password')

class Config():
  def __init__(self):
    self.config_dir = user_config_dir(APPNAME)
    self.init_config_dir()
    
    self.cred_path = os.path.join(self.config_dir, 'cred')
    self.cred = None
    self.load_cred()

    self.remember_path = os.path.join(self.config_dir, 'remember')

    self.input_path = os.path.join(self.config_dir, 'input.conf')
    self.write_keymap()

    self.min_unseen = MIN_UNSEEN
    self.more_unseen = MORE_UNSEEN
    self.limit = LIMIT
  
  def init_config_dir(self):
    if not os.path.exists(self.config_dir):
      print(f'{APPNAME}: No configuration directory found. Creating: {self.config_dir}')
      os.mkdir(self.config_dir)
  
  def load_cred(self):
    if not os.path.exists(self.cred_path):
      print(f'{APPNAME}: No credentials found.')
      
      user = input("Please enter your Lemmy username: ")
      password = getpass.getpass(prompt="Please enter your Lemmy password: ")

      self.cred = Cred({
        'user': user,
        'password': base64.b64encode(password.encode())
      })

      print(f'\n{APPNAME}: You can have your credentials saved to disk, if you like. In case you do your password will be stored obfuscated in the file {self.cred_path}.')
      save = input("Type YES to confirm saving credentials to file: ")

      if save == "YES":
        with open(self.cred_path, 'wb') as f:
          pickle.dump(self.cred, f)

    else:

      with open(self.cred_path, 'rb') as f:
        self.cred = pickle.load(f)

  def write_keymap(self):
    if not os.path.exists(self.input_path):
      with open(self.input_path, 'w') as f:
        f.write('# Next:\n')
        f.write('> quit 0\n')

        f.write('# Quit:\n')
        f.write('q quit 4\n')

        f.write('# Remember:\n')  # TODO???
        f.write('R run "/bin/bash" "-c" "echo \\\"${title}: ${path}\\\" >> ' + f'{self.remember_path}"\n')

        f.write('# Display info:\n')  # TODO???
        f.write('i show-text "${title}"\n')

        f.write('# Open youtube link:\n') # TODO???
        f.write('Ctrl+o run "/bin/bash" "-c" "xdg-open \\\"${path}\\\""\n')
        f.write('0xf run "/bin/bash" "-c" "xdg-open \\\"${path}\\\""\n')    # from cmd

        f.write('# Hold on to link for later:\n')
        f.write('Ctrl+h quit 5\n')

        f.write('# Download:\n')
        f.write('Ctrl+d quit 6\n')
        f.write('0x4 quit 6\n')     # from cmd

        f.write('# Loop link to restart:\n')
        f.write('Ctrl+l quit 1\n')
        f.write('0xc quit 1\n')     # from cmd

        f.write('# Exclude link:\n')
        f.write('Ctrl+x quit 7\n')
        f.write('0x18 quit 7\n')    # from cmd

        f.write('# Also:\n')
        f.write('# "n" shows number of links left\n') # TODO
        f.write('# "Ctrl+r" opens reddit link\n')     # TODO

class Lemidio():
    def __init__(self, instance="lemmy.world", community="music"):
      self.config = Config()

      self.instance = instance
      self.community = community

      self.save_path = os.path.join(
        self.config.config_dir,
        self.instance,
        self.community,
      )

      self.srv = None
      self.init_srv()

      self.known_ids = set()

      self.unseen = []
      self.seen = []
      self.trash = []
      if os.path.exists(self.save_path):
        self.load()
      if len(self.unseen) == 0:
        self.init_unseen()

    def init_srv(self):
      self.srv = LemmyHttp(f"https://{self.instance}")
      response = self.srv.login(
        self.config.cred.user,
        base64.b64decode(self.config.cred.password).decode()
      )
      if response.status_code == 200:
        print(f'{APPNAME}: Login successful.')
      else:
        print(f'{APPNAME}: Login failed.')
        exit(1)

    def init_unseen(self):
      self.unseen = self.get_more_unseen()
      if len(self.known_ids) == 0:
        self.known_ids = set([ e.get('post').get('id') for e in self.unseen ])

    def get_more_unseen(self):
      more_unseen = []
      page_index = 1
      while len(more_unseen) < self.config.more_unseen:
        api_response = self.srv.get_posts(community_name=self.community, limit=self.config.limit, page=page_index)
        if api_response.status_code != 200:
          print(f'get_more_unseen() got bad http code: {api_response.status_code}')
          break

        new_unseen = api_response.json().get('posts')
        if new_unseen is None:
          print(f'get_more_unseen() got not posts.')
          break

        new_unseen = [ e for e in new_unseen if e.get('post').get('id') not in self.known_ids ]

        more_unseen += new_unseen
        page_index += 1

      return more_unseen

    def play(self):
      while True:
        print(f"\n\t XXXXXXXXX status: unseen '{len(self.unseen)}'; seen '{len(self.seen)}'; trash '{len(self.trash)}'\n")
        current = self.unseen.pop(0)
        result = self.play_current(current)

        match result:
          case 0:
            # next '>'
            print(f"{APPNAME}: Action: Playing next item")
            self.seen.append(current)
          case 1:
            # retry play 'ctrl + l'
            print(f"{APPNAME}: Action: Retrying to play the current media")
            self.unseen.insert(0, current)
            continue
          case 2:
            # failure to play video
            print(f"{APPNAME}: Action: Media failed to play")
            self.trash.append(current)
          case 4:
            # soft quit 'q'
            print(f"{APPNAME}: Action: Stop playing media")
            self.unseen.insert(0, current)
            break
          case 5:
            # hold 'ctrl + h'
            print(f"{APPNAME}: Action: Holding current media for later")
            self.unseen.append(current)
          case 6:
            # download 'ctrl + d'
            # TODO
            print(f"{APPNAME}: download current video no yet supported...")
            self.unseen.insert(0, current)
          case 7:
            # exclude 'ctrl + x'
            print(f"{APPNAME}: Action: Excluding current media from any play")
            self.trash.append(current)
          case r:
            print(f"{APPNAME}: Unsupported return code: {r}")

        if len(self.unseen) < self.config.min_unseen:
          more_unseen = self.get_more_unseen()
          self.unseen += more_unseen
          self.known_ids = self.known_ids.union(set([ e.get('post').get('id') for e in more_unseen ]))

    def play_current(self, current):
      url = current.get('post').get('url')
      if url is None:
        # equivalent to automatic failure to play video
        return 2

      p = subprocess.Popen(
        [
          'mpv',
          url,
          '--input-conf=%s' % self.config.input_path,
        ]
        , shell=False)

      p.communicate()

      return p.returncode

    def save(self):
      if not os.path.exists(self.save_path):
        os.makedirs(self.save_path)

      for store in ('unseen', 'seen', 'trash'):
        print(f'{APPNAME}: Saving {store}...')

        with gzip.open(os.path.join(self.save_path, store + '.json.gz'), 'w') as f:
          f.write(json.dumps(self.__dict__[store]).encode('utf-8'))

      with gzip.open(os.path.join(self.save_path, 'ids.gz'), 'wb') as f:
        pickle.dump(self.known_ids, f)

    def load(self):
      if not os.path.exists(self.save_path):
        return

      for store in ('unseen', 'seen', 'trash'):
        print(f'{APPNAME}: Loading {store}...')

        file_name = os.path.join(self.save_path, store + '.json.gz')

        if os.path.exists(file_name):
          with gzip.open(file_name, 'r') as f:
            self.__dict__[store] = json.loads(f.read().decode('utf-8'))

      file_name = os.path.join(self.save_path, 'ids.gz')
      if os.path.exists(file_name):
        with gzip.open(file_name, 'rb') as f:
          self.known_ids = pickle.load(f)
      else:
        self.known_ids = set().union(
          set([ e.get('post').get('id') for e in self.unseen ])
        ).union(
          set([ e.get('post').get('id') for e in self.seen ])
        ).union(
          set([ e.get('post').get('id') for e in self.trash ])
        )

if __name__ == '__main__':

  rl = Lemidio()

  rl.play()

  rl.save()

  print(f'{APPNAME}: Reached end. Exiting.')
  exit(0)
